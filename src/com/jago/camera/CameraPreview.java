package com.jago.camera;

import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.opengl.GLES20;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

	private static final int ROTATE_DEGREES = 0;
	private String TAG = "TAG";
	private SurfaceHolder mHolder = null;
	private Camera mCamera = null;
	
	private class MySize {
		public MySize(int width2, int height2) {
			width=width2;
			height=height2;
		}
		int width;
		int height;
	}
	
	private MySize mPreviewSize;


	public CameraPreview(Context context) {
		super(context);
	}

	public CameraPreview(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CameraPreview(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}


	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (mCamera == null)  {
			resumeCamera();
			return;
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// empty. Take care of releasing the Camera preview in your activity.
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// If your preview can change or rotate, take care of those events here.
		// Make sure to stop the preview before resizing or reformatting it.
		if (mHolder.getSurface() == null){
			// preview surface does not exist
			return;
		}

		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e){
			// ignore: tried to stop a non-existent preview
		}
		// set preview size and make any resize, rotate or
		// reformatting changes here
		try {
		Camera.Parameters p = mCamera.getParameters();
		Camera.Size s = getBestPreviewSize(w, h, p);
		p.setPreviewSize(s.width,  s.height);

		mCamera.setParameters(p);
		/*ViewGroup.LayoutParams lp = this.getLayoutParams();
		lp.height = s.height;
		lp.width = s.width;

		this.setLayoutParams(lp);*/
		
		}
		catch (Exception e) {
			
		}
		
		// start preview with new settings
		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();

		} catch (Exception e){
			Log.d(TAG, "Error starting camera preview: " + e.getMessage());
		}
	}

	public static Camera getCameraInstance(){
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
		}
		catch (Exception e){
			// Camera is not available (in use or does not exist)
		}
		return c; // returns null if camera is unavailable
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
	    if (changed) {
	        final int width = r - l;
	        final int height = b - t;

	        int previewWidth = width;
	        int previewHeight = height;
	        if (mPreviewSize != null) {
	            previewWidth = mPreviewSize.width;
	            previewHeight = mPreviewSize.height;
	            mPreviewSize.width = width;
	            mPreviewSize.height = height;
	        }

	        // Center the child SurfaceView within the parent.
	        if (width * previewHeight < height * previewWidth) {
	            final int scaledChildWidth = previewWidth * height / previewHeight;
	            this.layout((width - scaledChildWidth) / 2, 0,
	                    (width + scaledChildWidth) / 2, height);
	        } else {
	            final int scaledChildHeight = previewHeight * width / previewWidth;
	            this.layout(0, (height - scaledChildHeight) / 2,
	                    width, (height + scaledChildHeight) / 2);
	        }
	    }
	}


	private void releaseCamera(){
		if (mCamera != null){
			mCamera.stopPreview();  
			mCamera.setPreviewCallback(null);    
			mCamera.release();     
			mCamera = null;     
		}
		GLES20.glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	}

	private void resumeCamera(){
		if (mCamera == null){
			mCamera = getCameraInstance();

			// Install a SurfaceHolder.Callback so we get notified when the
			// underlying surface is created and destroyed.
			mHolder = getHolder();
			mHolder.addCallback(this);
			// deprecated setting, but required on Android versions prior to 3.0
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

			mCamera.setDisplayOrientation(ROTATE_DEGREES);
			// The Surface has been created, now tell the camera where to draw the preview.
			try {
				mCamera.setPreviewDisplay(mHolder);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mCamera.startPreview();

		}
	}

	public void onPause() {
		releaseCamera();
	}

	public void onResume() {
		resumeCamera();
	}

	private Camera.Size getBestPreviewSize(int width, int height,
			Camera.Parameters parameters) {
		Camera.Size result=null;

		for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
			if (size.width <= width && size.height <= height) {
				if (result == null) {
					result=size;
				}
				else {
					int resultArea=result.width * result.height;
					int newArea=size.width * size.height;

					if (newArea > resultArea) {
						result=size;
					}
				}
			}
		}

		return(result);
	}


}
