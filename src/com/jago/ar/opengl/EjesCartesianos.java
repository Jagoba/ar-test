package com.jago.ar.opengl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.GLES20;

public class EjesCartesianos {
	private final String vertexShaderCode =
			"uniform mat4 uMVPMatrix;" + 
			"attribute vec4 vPosition;" +
			"void main() {" +
			"  gl_Position = uMVPMatrix * vPosition;" +
			"}";
	
	private final String fragmentShaderCode =
			"precision mediump float;" +
					"uniform vec4 vColor;" +
					"void main() {" +
					"  gl_FragColor = vColor;" +
					"}";


	private FloatBuffer vertexBuffer;

	// number of coordinates per vertex in this array
	static final int COORDS_PER_VERTEX = 3;
	private final float coordsX[] = {  
			 10.0f, 0.0f, 0.0f, 
			0.0f, 0.0f, 0.0f,
	};
	
	private final float coordsY[] = { 
			0.0f,  10.0f, 0.0f, 
			0.0f, 0.0f, 0.0f,
	};
	private final float coordsZ[] = {  
			0.0f, 0.0f,  10.0f, 
			0.0f, 0.0f, 0.0f,
	};

	// Set color with red, green, blue and alpha (opacity) values
	float color[] = { 
			1.0f, 0.0f, 0.0f, 1.0f, //xColor
			0.0f, 1.0f, 0.0f, 1.0f, //yColor
			0.0f, 0.0f, 1.0f, 1.0f  //zColor
		};

	private int mProgram;

	private int mPositionHandle;

	private int vertexStride;

	private int mColorHandle;

	public EjesCartesianos() {
		// initialize vertex byte buffer for shape coordinates
		ByteBuffer bb = ByteBuffer.allocateDirect(
				// (number of coordinate values * 3 coordinates * 4 bytes per float)
				coordsX.length * 3 * 4);
		// use the device hardware's native byte order
		bb.order(ByteOrder.nativeOrder());

		// create a floating point buffer from the ByteBuffer
		vertexBuffer = bb.asFloatBuffer();
		// add the coordinates to the FloatBuffer
		vertexBuffer.put(coordsX);
		vertexBuffer.put(coordsY);
		vertexBuffer.put(coordsZ);
		// set the buffer to read the first coordinate
		vertexBuffer.position(0);

		int vertexShader = MyRenderer.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
		int fragmentShader = MyRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

		mProgram = GLES20.glCreateProgram();             // create empty OpenGL ES Program
		GLES20.glAttachShader(mProgram, vertexShader);   // add the vertex shader to program
		GLES20.glAttachShader(mProgram, fragmentShader); // add the fragment shader to program
		GLES20.glLinkProgram(mProgram);                  // creates OpenGL ES program executables
		
		
	}

	public void draw(float[] mvpMatrix) {
		// Add program to OpenGL ES environment
		GLES20.glUseProgram(mProgram);

		// get handle to vertex shader's vPosition member
		mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

		// Enable a handle to the triangle vertices
		GLES20.glEnableVertexAttribArray(mPositionHandle);

		// Prepare the triangle coordinate data
		GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false,
				vertexStride, vertexBuffer);

		// get handle to fragment shader's vColor member
		mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");


		// get handle to shape's transformation matrix
		int mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

		// Pass the projection and view transformation to the shader
		GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);

		// Set color for drawing
		GLES20.glUniform4fv(mColorHandle, 1, color, 0);

		// Draw the triangle
		GLES20.glDrawArrays(GLES20.GL_LINES, 0, 2);
		// Set color for drawing
		GLES20.glUniform4fv(mColorHandle, 1, color, 4);

		GLES20.glDrawArrays(GLES20.GL_LINES, 2, 2);
		// Set color for drawing
		GLES20.glUniform4fv(mColorHandle, 1, color, 8);

		GLES20.glDrawArrays(GLES20.GL_LINES, 4, 2);

		// Disable vertex array
		GLES20.glDisableVertexAttribArray(mPositionHandle);
	}


}
