package com.jago.ar.opengl;

import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.widget.Toast;

public class AR_GLSurfaceView extends GLSurfaceView implements SensorEventListener {

	private SensorManager mSensorManager;
	private MyRenderer mRenderer;
	private Sensor mRotationVector;
	float[] rotVector;


	final float PI = (float) Math.PI;
	final float rad2deg = 180/PI;    


	public AR_GLSurfaceView(Context context) {
		super(context);
		initialize();
	}

	public AR_GLSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	private void initialize() {
		mRenderer = new MyRenderer();
		setEGLContextClientVersion(2);
		getHolder().setFormat(PixelFormat.RGBA_8888);
		setZOrderOnTop(true);
		setEGLConfigChooser(8, 8, 8, 8, 16, 0);
		this.setRenderer(mRenderer);
		this.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		mSensorManager = (SensorManager)getContext().getSystemService(Context.SENSOR_SERVICE);
		mRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
	}

	@Override
	public void onResume() {
		super.onResume();

		boolean supported = mSensorManager.registerListener(this, mRotationVector, SensorManager.SENSOR_DELAY_FASTEST);
		if (!supported) Toast.makeText(getContext(), "Rotation_Vector not supported", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		float alpha = 0.7f;
		if(event.sensor.getType()==Sensor.TYPE_ROTATION_VECTOR) {
			if (rotVector == null)
				rotVector = new float[event.values.length];
			for (int i = 0; i < event.values.length; i++) {
				rotVector[i] = rotVector[i]*(1-alpha) + event.values[i]*alpha;
			}
			float[] R = new float[16];

			SensorManager.getRotationMatrixFromVector(R, rotVector);
			float[] remapR = new float[16];

			SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, remapR);
			
			//Mapear direccion movil = opengl Camera
			mRenderer.setRotationMatrix(remapR);

			requestRender();
		}

	}
}
