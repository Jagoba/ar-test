package com.jago.ar.opengl;

import android.util.Log;

public class ARCamera {

	public float eyeX, eyeY, eyeZ, lookAtX, lookAtY, lookAtZ, upX, upY, upZ;

	public float aperture;

	public float near, far;


	public ARCamera() {

		this.eyeX = 0.0f;
		this.eyeY = 1.3f;
		this.eyeZ = 4.0f;
		this.lookAtX = 0.0f;
		this.lookAtY = -1.0f;
		this.lookAtZ = 0.0f;
		this.upX = 0.0f;
		this.upY = 0.0f;
		this.upZ = -1.0f;
		aperture =  0.6f;
		near = 1.0f;
		far = 20.0f;
	}

	public void pitch (float angle) {
	}


	public void print() {
		Log.d("CAMERA", "eye: ( "+eyeX+", "+eyeY+", "+eyeZ+")");
		Log.d("CAMERA", "lookAt: ( "+lookAtX+", "+lookAtY+", "+lookAtZ+")");
		Log.d("CAMERA", "up: ( "+upX+", "+upY+", "+upZ+")");
		Log.d("CAMERA", "aperture: "+aperture);

		Log.d("CAMERA", "near: "+near);
		Log.d("CAMERA", "far: "+far);
	}
	
}
