package com.jago.ar.opengl;


import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

public class MyRenderer implements GLSurfaceView.Renderer {

	private ARCamera mCamera;

	private EjesCartesianos mAxis;
	private FloorGrid mGrid;
	private Triangle mTriangle;
	private float[] mViewMatrix;
	private float[] mMVPMatrix;
	private float[] mProjectionMatrix;
	float[] mRotationMatrix;


	@Override
	public void onSurfaceCreated(GL10 unused, EGLConfig config) {

		mProjectionMatrix = new float[16];
		mViewMatrix = new float[16];
		mMVPMatrix = new float[16];
		mRotationMatrix = new float[16];
		mAxis = new EjesCartesianos();
		mTriangle = new Triangle();
		mGrid = new FloorGrid();

		//Set clear color
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
		GLES20.glEnable( GLES20.GL_CULL_FACE );
		

		//Enable depth, z-buffer
		GLES20.glEnable( GLES20.GL_DEPTH_TEST );
		GLES20.glDepthFunc( GLES20.GL_LEQUAL );
		GLES20.glDepthMask( true );
		mCamera = new ARCamera();
	}



	@Override
	public void onDrawFrame(GL10 unused) {
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
		// Set the camera position (View matrix)



		Matrix.setLookAtM(mViewMatrix, 0, 
				mCamera.eyeX, mCamera.eyeY, mCamera.eyeZ, 
				mCamera.lookAtX+mCamera.eyeX, mCamera.lookAtY+mCamera.eyeY, mCamera.lookAtZ+mCamera.eyeZ, 
				mCamera.upX, mCamera.upY, mCamera.upZ);
		
		// Calculate the projection and view transformation
		Matrix.multiplyMM(mMVPMatrix, 0,mRotationMatrix, 0, mViewMatrix, 0);
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
		// Draw shape
		GLES20.glLineWidth(3.0f);
		mGrid.draw(mMVPMatrix);
		mTriangle.draw(mMVPMatrix);
		GLES20.glLineWidth(5.0f);
		mAxis.draw(mMVPMatrix);

	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		GLES20.glViewport(0, 0, width, height);		

		float ratio = (float) width / height;

		// this projection matrix is applied to object coordinates
		// in the onDrawFrame() method
		float left, right, top, bottom, near, far;
		near = mCamera.near;
		far = mCamera.far;
		top = near*(mCamera.aperture);
		bottom = -top;
		right = ratio*top;
		left = -right;

		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
	}

	public static int loadShader(int type, String shaderCode){

		// create a vertex shader type (GLES20.GL_VERTEX_SHADER)
		// or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
		int shader = GLES20.glCreateShader(type);

		// add the source code to the shader and compile it
		GLES20.glShaderSource(shader, shaderCode);
		GLES20.glCompileShader(shader);

		return shader;
	}

	public void sensorChanged(float rollX, float pitchY, float yawZ) {
		if (mCamera != null) {
			mCamera.pitch(pitchY);
		}
	}


	public void setRotationMatrix(float[] remapR) {
		if (mRotationMatrix == null) return;
		mRotationMatrix = remapR;
	}



	public void step() {
		mCamera.eyeZ--;
	}
}
