package com.jago.ar.test;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.jago.ar.opengl.AR_GLSurfaceView;
import com.jago.camera.CameraPreview;

public class MainActivity extends Activity {

	//OpenGL
	private AR_GLSurfaceView mGLView;

	//Camera
	private CameraPreview mPreview;

	//Activity
	private View decorView;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//Activity
		decorView = this.getWindow().getDecorView();

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB)
			decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
				@Override
				public void onSystemUiVisibilityChange(int visibility) {
					if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
						hideUIControlls();
					}
				}
			});

		//OpenGL
		mGLView = (AR_GLSurfaceView) findViewById(R.id.open_gl_surface);
		//Camera
		mPreview = (CameraPreview) findViewById(R.id.camera_surface);
	}

	//Activity
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			hideUIControlls();
		}
	}

	@TargetApi(19)
	private void hideUIControlls() {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
			decorView.setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mPreview != null)
			mPreview.onPause();
		if (mGLView != null)
			mGLView.onPause();
	}
	@Override
	protected void onResume() {
		super.onResume();
		if (mPreview != null)
			mPreview.onResume();
		if (mGLView != null)
			mGLView.onResume();
	}

}
